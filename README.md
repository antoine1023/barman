# README

Barman is an IRC bot used on [SmoothIRC](http://smoothirc.net/) in order to
serve people some delicious fruit juices.

Because of this IRC network is mainly French, all fruits and setences
are also in this language.

## Dependencies

To install all dependencies, simply run `pip install -r requirements.txt`.

## Copyright

Barman is brought to you under the WTFPL licence. For further informations
read the LICENCE file.
