# coding: utf-8

# By MicroJoe <microjoe@mailoo.org>, WTFPL pwred.

import sys
from random import choice

from twisted.words.protocols import irc
from twisted.internet import protocol, reactor


##############################################################################


class Product:

    def __init__(self, klass, name):
        self.klass = klass
        self.name = name

    def __str__(self):
        return self.klass.name + " " + self.name

    def __repr__(self):
        return "<Product klass=" + self.klass.name + " name=" + self.name + ">"

##############################################################################


class ProductClass:

    def __init__(self, name, serving_sentence, products = None):

        self.name = name
        self.serving_sentence = serving_sentence

        if products == None:
            products = []
        self.products = products



    def __str__(self):
        return "<ProductClass (" + name + ")>"


def read_product_class(name, serving_sentence):

    klass = ProductClass(name, serving_sentence)

    filename = name + ".txt"

    with open(filename, 'r') as fp:
        lines = fp.read().split('\n')

    for line in lines:
        line = line.strip()
        if len(line) != 0:
            klass.products.append(Product(klass, line))

    return klass

##############################################################################


class Bar:

    def __init__(self, product_classes, admin):
        self.product_classes = product_classes
        self.products = []
        for product_class in product_classes:
            self.products += product_class.products
        self.banned_nicks = []
        self.admin = admin


    def print_products(self):
        s = "Voici tout ce que j’ai :\n.\n"
        for product_class in self.product_classes:
            s += self.print_product_class(product_class) + "\n.\n"
        return s


    def print_product_class(self, product_class):
        s = "#### " + product_class.name + " ####\n"
        for product in product_class.products:
            s += product.name + ", "
        return s


    def get_products_from_name(self, name):
        results = []
        for product in self.products:
            if name in product.name:
                results.append(product)
        return results


    def get_product_class_from_name(self, name):
        for klass in self.product_classes:
            if klass.name == name:
                return klass
        return None


    def ban(self, nick):
        self.banned_nicks.append(nick.lower())


    def unban(self, nick):
        self.banned_nicks.remove(nick.lower())


    def is_banned(self, nick):
        return nick.lower() in self.banned_nicks


##############################################################################


class Barman(irc.IRCClient):

    def _get_nickname(self):
        return self.factory.nickname

    nickname = property(_get_nickname)


    def get_nick_from_user(self, user):
        return user.split('!')[0]


    def signedOn(self):
        self.join(self.factory.channel)
        print 'Signed in as ' + self.nickname
        self.describe(self.factory.channel, "Bonjour le peuple ! " +
            "Dites-moi `!liste` pour tout savoir sur les consommations.")


    def print_product_list(self, user, channel):
        bar = self.factory.bar
        message = bar.print_products()
        #lines = message.splitlines()
        #for line in lines:
        self.msg(self.get_nick_from_user(user), message)


    def privmsg(self, user, channel, msg):

        bar = self.factory.bar

        if not user:
            return

        if not msg.startswith('!'):
            return

        user_nick = self.get_nick_from_user(user)

        if bar.is_banned(user_nick):
            return

        cmd_str = msg.split()[0][1:].lower()

        if cmd_str == "liste":
            self.print_product_list(user, channel)
            return

        if user_nick == bar.admin and len(msg.split()) == 2:
            target_nick = msg.split()[1].lower()
            if cmd_str == "ban":
                bar.ban(target_nick)
                print target_nick + " banned"
                return
            elif cmd_str == "unban":
                bar.unban(target_nick)
                print target_nick + " unbanned"
                return

        cmd_str = cmd_str.replace("_", " ")

        product = None
        products = bar.get_products_from_name(cmd_str)
        if len(products) == 0:
            product_class = bar.get_product_class_from_name(cmd_str)
            if product_class is None:
                return
            product = choice(product_class.products)
        else:
            product = choice(products)

        users = msg.split()[1:]

        # If no users specified, we serve the initial msg user
        if not users:
            users = [user_nick]

        # Antispam if more than 5 people to serve
        if len(users) > 5:
            self.msg(self.factory.channel,
                     "Y’a trop de monde à servir, chacun son tour.")
            return

        # Serve random juices
        for user in users:
            msg = product.klass.serving_sentence + " " + \
                product.name + " à " + user
            print msg
            self.describe(self.factory.channel, msg)


##############################################################################


class BarmanFactory(protocol.ClientFactory):
    protocol = Barman

    def __init__(self, channel, nickname, bar, admin):
        self.channel = channel
        self.nickname = nickname
        self.bar = bar
        self.admin = admin


    def clientConnectionLost(self, connector, reason):
        print 'Connection lost : %s, reconnecting' % reason
        connector.connect()


    def clientConnectionFailed(self, connector, reason):
        print 'Could not connect : ' % reason


##############################################################################


def load_bar(admin):

    product_classes = [
        read_product_class('boisson', 'sert'),
        read_product_class('jus', 'sert un jus'),
        read_product_class('glace', 'sert une glace'),
        read_product_class('sorbet', 'sert un sorbet'),
    ]

    print "Products loaded"

    return Bar(product_classes, admin)



if __name__ == '__main__':

    if (len(sys.argv) != 4):
        print "Usage : barman <nickname> <chan> <admin>"
        sys.exit(1)

    nickname = chan = sys.argv[1]
    chan = sys.argv[2]
    admin = sys.argv[3]
    bar = load_bar(admin)
    bf = BarmanFactory('#' + chan, nickname, bar, admin)

    reactor.connectTCP('irc.smoothirc.net', 6667, bf)
    reactor.run()


